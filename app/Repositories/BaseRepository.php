<?php

namespace App\Repositories;

use App\Interfaces\IbaseRepository;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements IbaseRepository
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function myGet(){
        return $this->model->get();

    }
    public function myFind($id){

        $data = $this->model->find($id);
        if (!$data) {
            flash('No data found')->error();
            return null;
        } else {
            return $data ;
        }
        
    }
    public function myDelete($id){
        $data = $this->model->find($id);
        if (!$data) {
            flash('No item found')->error();
        } 
        else {
            $data->delete();
        }
        
    }
}
