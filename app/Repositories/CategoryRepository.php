<?php

namespace App\Repositories;

use App\Interfaces\ICategoryRepository;
use App\Models\Category;

class CategoryRepository extends BaseRepository implements ICategoryRepository
{
    

    public function __construct(Category $model)
    {
        parent::__construct($model);
    }
    public function CreateCategory($request){
        // $category = new Category();
        $category = $this->model;
        $category->name= $request->name;
        $category->main_category_id = $request->main_category_id;
        $category->save();
        flash('Successfully created')->success();

    }


}
