<?php

namespace App\Interfaces;

interface ICategoryRepository extends IbaseRepository
{
    public function CreateCategory($request);
}
