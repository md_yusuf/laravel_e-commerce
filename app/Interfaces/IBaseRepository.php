<?php

namespace App\Interfaces;

interface IbaseRepository
{
    public function myGet();
    public function myFind($id);
    public function myDelete($id);
}
